<?php namespace Polylab\Entities;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider;

class EntitiesServiceProvider extends AuthServiceProvider
{
    public function register()
    {
        $this->app->bind('ExampleEntity', function ($oApp) {
            return new Models\ExampleEntity();
        });

        /**
         * Group entities into an Service Container group, refer to Entities\Models\Entities for more information on how this is set up
         */
        $this->app->tag(['ExampleEntity'], 'entities');

        //Make binding that will return all entities from the tagged group
        $this->app->bind('EntityTypes', function ($oApp) {
            return new Models\EntityTypes($oApp);
        });
    }

    public function boot(Gate $gate)
    {
        $this->_registerViews();
        $this->_registerPermissions($gate);
        $this->_registerPublishables();
    }

    private function _registerPermissions(Gate $gate) {
        //Define Permissions and Access Control
        $this->registerPolicies($gate);


        $permissions = [
            'see-entities',
            'toggle-entities'
        ];

        foreach($permissions as $permission) {
            $gate->define($permission, function($user, $permission, $gate) {
                //If super admin, bypass the rest of the validation
                $gate->before(function ($user) {
                    if ($user->getPermissionValue('super-admin')) {
                        return true;
                    }
                });

                return $user->getPermissionValue($permission);
            });
        }
    }

    private function _registerViews() {
        $this->loadViewsFrom(__DIR__.'/views', 'entities');
    }

    private function _registerPublishables() {
        $this->publishes([
            realpath(__DIR__.'/migrations') => $this->app->databasePath().'/migrations',
        ]);
    }
}