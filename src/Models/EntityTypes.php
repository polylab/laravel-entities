<?php namespace Polylab\Entities\Models;

use \Illuminate\Foundation\Application as App;

/**
 * Class Entities
 * @package Polylab\Entities\Models
 *
 * Aggregates all types of entities by resolving them through an IoC tagged group called entities.
 * If you create an entity, you will need to add it to this tagged group for it to be resolved, so that it will
 * play nicely with the extended features of an entity, such as it being able to be administered inside
 * the polylab/laravel-dashboard module, or any other module that relies on this one.
 */
class EntityTypes
{
    public function __construct(App $oApp) {
        $this->app = $oApp;
    }

    public function getEntities() {
        return $this->app->tagged('entities');
    }

    public function getEntity($vEntity) {
        return $this->app->make($vEntity);
    }
}