<?php namespace Polylab\Entities\Models;

use \Polylab\Entities\Traits\IsEntity;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class ExampleEntity
 * @package Polylab\Entities\Models
 *
 * This is an example implementation of a basic entity.
 */
class ExampleEntity extends Model
{
    /**
     * Including the IsEntity trait gives your entity some base functionality to allow
     * it to interact reliably with modules that depend on the polylab/laravel-entities module.
     */
    use IsEntity;

    /**
     * @var array
     *
     * Default Laravel Behavior, these attributes are fillable during the model's creation and
     * available inside the Models JSON form.
     */
    protected $fillable = [
        'name',
        'color',
        'size'
    ];

    /**
     * @var array
     *
     * Core Laravel behavior, these attributes are not fillable during the model's creation
     * but they are available inside the Models JSON form.
     */
    protected $guarded = [
        'id'
    ];

    /**
     * @var array
     *
     * This provides a way to set which attributes should be viewable inside front end templates.
     * Retrieve a key value array of the attributes from the model using $this->getFrontAttributes();
     */
    protected $frontAttributes = [
        'name',
        'color',
        'size'
    ];
}