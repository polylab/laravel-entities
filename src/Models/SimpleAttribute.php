<?php namespace Polylab\Entities\Models;

/**
 * This class may seem too simple, but it's to illustrate that entities
 * can have different types of attributes. A ComplexAttribute could
 * handle the fetching of data for an EAV data structure for example
 *
 * Class SimpleAttribute
 * @package Polylab\Entities\Models
 */
class SimpleAttribute
{
    public function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
    }
}