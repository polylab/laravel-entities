<?php namespace Polylab\Entities\Traits;

trait IsEntity
{
    public $isEntity = true;

    /**
     * @return string
     *
     * Returns the model name, if one is not manually set on the class it uses reflection
     * to get the name of the concrete class to use as the model name.
     */
    public function getModelName() {
        $oReflection = new \ReflectionClass($this);
        return isset($this->modelName) ? $this->modelName : $oReflection->getShortName();
    }

    /**
     * @return string
     *
     * Returns the plural of the model name.
     */
    public function getModelNamePlural() {
        return isset($this->modelNamePlural) ? $this->modelNamePlural : $this->getModelName() . 's';
    }

    /**
     * @param $vTemplate
     * @return string
     *
     * Gets a template view path from the model, if one is not set use this modules default templates.
     *
     * An example would be a list template to show a list of this particular
     * type of model. Inside your concrete implementation of the model you would
     * specify a protected variable called listTemplate, with the view path, eg: 'mytheme.myentity.list'
     * Then when the model is used by other parts of your application you can simply
     * call $yourEntity->getTemplate('list') to get the list template path.
     *
     */
    public function getTemplate($vTemplate) {
        $templateProperty = $vTemplate.'Template';
        return isset($this->{$templateProperty})
            ? $this->{$templateProperty}
            : 'entities::entities.default.' . $vTemplate;
    }

    /**
     * @return array
     *
     * Returns a key value array of all the attributes meant for the front end.
     * If $this->frontAttributes is set, it will return the attributes listed in
     * that array. Otherwise it will return the attributes listed in $this->fillable
     * and $this->guarded.
     *
     * You can of course completely skip using this and create a custom template for
     * the entity, then manipulate and format it however you wish.
     */
    public function getFrontAttributes() {
        $aAttributes = [];
        $aReturn = [];

        if(isset($this->fillable)) $aAttributes = $this->fillable;
        if(isset($this->guarded)) $aAttributes = array_merge($aAttributes, $this->guarded);

        if(is_array($this->frontAttributes)) $aAttributes = $this->frontAttributes;

        foreach($aAttributes as $vAttributeName) {
            if($vAttributeName === '*') continue;

            $aReturn[$vAttributeName] = $this->{$vAttributeName};
        }

        return $aReturn;
    }
}