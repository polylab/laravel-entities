<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExampleentityTable extends Migration
{
    public function up()
    {
        if(Schema::hasTable('example_entities')) return;

        Schema::create('example_entities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('color');
            $table->string('size');
            $table->timestamps();
        });
    }
}
