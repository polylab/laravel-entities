<div class="entity-list-container {{strtolower($modelInstance->getModelName())}}-list">
    <div class="inner-container">
        <h4 class="title">{{$modelInstance->getModelNamePlural()}}</h4>
        <div class="entities">
            @foreach($entities as $entity)
            <div class="entity">
                <div class="attributes">
                    @foreach($entity->getFrontAttributes() as $name => $value)
                        <div class="attribute {{ strtolower($name) }}-attribute">
                            <div class="name">{{ ucfirst($name)}}</div>
                            <div class="value">{{$value ? $value : "Not Set"}}</div>
                        </div>
                    @endforeach
                </div>
                <a class="view-entity" href="/{{$entity->getModelName()}}/view/{{$entity->id}}">View</a>
            </div>
            @endforeach
        </div>
    </div>
</div>