<div class="entity-view">
    <div class="inner-container">
        <div class="entity-name">{{ $entity->name }}</div>
        <div class="attribute-title">Attributes</div>
        <div class="attributes">
            @foreach($entity->getFrontAttributes() as $attributeName => $attributeValue)
            <div class="attribute">
                <div class="attribute-name">{{ $attributeName }}</div>
                <div class="attribute-value">{{ $attributeValue }}</div>
            </div>
            @endforeach
        </div>
    </div>
</div>